import React from 'react';
import {StyleSheet, View, TouchableOpacity, ImageBackground, Alert, AsyncStorage} from 'react-native';
import {Card, CardItem, Text, Icon, Button, Item} from 'native-base'
import Dimensions from 'Dimensions';
import axios from "axios";
import Constants from "../../constants/Variables";

const url = Constants.url;
const DEVICE_HEIGHT = Dimensions.get('window').height;


export default class Offer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rateClicked1: false,
            rateClicked2: false,
            rateClicked3: false,
            rateClicked4: false,
            rateClicked5: false,
            tipClicked1: false,
            tipClicked2: false,
            tipClicked3: false,
            tipClicked4: false,
            tipClicked5: false,
        };
    }

    componentDidMount() {
        this.checkRate();
    }

    async checkRate() {
        let deal = this.props.deal;
        axios.post(url + '/checkRate', {
            idClient: await AsyncStorage.getItem('client'),
            idDeal: deal[10]
        }).then(res => {
                if (res.status === 200) {
                    switch (parseInt(res.data[0].Rate)) {
                        case 1:
                            this.setState({rateClicked1: true});
                            break;
                        case 2:
                            this.setState({rateClicked2: true});
                            break;
                        case 3:
                            this.setState({rateClicked3: true});
                            break;
                        case 4:
                            this.setState({rateClicked4: true});
                            break;
                        case 5:
                            this.setState({rateClicked5: true});
                            break;
                    }
                }
            }
        )
    }

    async rate(rate) {
        if (this.state.rateClicked1 || this.state.rateClicked2 || this.state.rateClicked3 || this.state.rateClicked4 || this.state.rateClicked5) {
            Alert.alert("Ошибка", "Нельзя проголосовать дважды")
        } else {
            let deal = this.props.deal;
            axios.post(url + '/rate', {
                Rate: rate,
                idClient: await AsyncStorage.getItem('client'),
                idCard:
                    await
                        AsyncStorage.getItem('card'),
                idCompany:
                    deal[11],
                idDeal:
                    deal[10]
            }).then(res => {
                if (res.status === 200) {
                    switch (rate) {
                        case 1:
                            this.setState({rateClicked1: true});
                            break;
                        case 2:
                            this.setState({rateClicked2: true});
                            break;
                        case 3:
                            this.setState({rateClicked3: true});
                            break;
                        case 4:
                            this.setState({rateClicked4: true});
                            break;
                        case 5:
                            this.setState({rateClicked5: true});
                            break;
                    }
                }
            })
        }
    }

    async tip(tip) {
        if (this.state.tipClicked) {
            Alert.alert("Ошибка", "Нельзя дважды дать чаевые")
        } else {
            if (this.props.balance < tip) {
                Alert.alert("Ошибка", "Недостаточно средств")
            }
            else {
                Alert.alert(
                    'Подтверждение',
                    'Вы уверены,что хотите перевести ' + tip + ' бонусов?',
                    [
                        {text: 'Cancel', style: 'cancel',},
                        {
                            text: 'OK', onPress: () => {
                                this.sendTip(tip);
                            }
                        },
                    ],
                );

            }
        }
    }

    async sendTip(tip) {
        let deal = this.props.deal;
        axios.post(url + '/tip', {
            Tip: tip,
            idCard: await AsyncStorage.getItem('card'),
            idWorker:
                deal[12]
        }).then(res => {
            console.log(res.status);
            if (res.status === 200) {
                switch (tip) {
                    case 10:
                        this.setState({tipClicked1: true});
                        break;
                    case 30:
                        this.setState({tipClicked2: true});
                        break;
                    case 50:
                        this.setState({tipClicked3: true});
                        break;
                    case 100:
                        this.setState({tipClicked4: true});
                        break;
                    case 200:
                        this.setState({tipClicked5: true});
                        break;
                }
            }
        });
    }

    render() {
        let deal = this.props.deal;
        let call = this.props.hideme;
        return (
            <View style={styles.card}>
                <Card style={styles.modal}>

                    <CardItem header bordered style={styles.header}>
                        <Text>Покупка</Text>
                    </CardItem>
                    <CardItem bordered style={styles.cardbody}>
                        <Text style={{fontWeight: 'bold'}}>{deal[9]}</Text>
                        <Text style={{fontWeight: 'bold',paddingTop:10}}>{deal[2]} {deal[1]}</Text>
                        <Text style={{fontWeight: 'bold',paddingTop:10}}>Сумма {deal[3]} руб.</Text>
                        <Text style={{fontWeight: 'bold',paddingTop:10}}>Скидка -{deal[6]}% = {(deal[3] / 100) * deal[6]} руб</Text>
                        <Text>               {deal[0]}</Text>
                        <Text style={{fontWeight: 'bold',paddingTop:10}}>Бонус   <Text style={{fontWeight:'bold',color: 'red'}}>+{deal[4]}</Text></Text>

                        <View style={styles.rateAndTip}>
                            <Text style={{width: 65, paddingTop: 10}}>Оценка</Text>
                            <View style={{paddingLeft: 15, width: 250, flex: 1, flexDirection: 'row'}}>
                                <Button bordered style={this.state.rateClicked1 ? styles.btnRateClicked : styles.btnRate} onPress={() => {this.rate(1)}}><Text style={styles.btnTextRate}>1</Text></Button>
                                <Button bordered style={this.state.rateClicked2 ? styles.btnRateClicked : styles.btnRate} onPress={() => {this.rate(2)}}><Text style={styles.btnTextRate}>2</Text></Button>
                                <Button bordered style={this.state.rateClicked3 ? styles.btnRateClicked : styles.btnRate} onPress={() => {this.rate(3)}}><Text style={styles.btnTextRate}>3</Text></Button>
                                <Button bordered style={this.state.rateClicked4 ? styles.btnRateClicked : styles.btnRate} onPress={() => {this.rate(4)}}><Text style={styles.btnTextRate}>4</Text></Button>
                                <Button bordered style={this.state.rateClicked5 ? styles.btnRateClicked : styles.btnRate} onPress={() => {this.rate(5)}}><Text style={styles.btnTextRate}>5</Text></Button>
                            </View>
                        </View>


                        <View style={styles.rateAndTip}>
                            <Text style={{width: 65, paddingTop: 10}}>Чаевые</Text>
                            <View style={{paddingLeft: 15, width: 250, flex: 1, flexDirection: 'row'}}>
                                <Button bordered style={this.state.tipClicked1 ? styles.btnTipClicked : styles.btnTip} onPress={() => {this.tip(10)}}><Text style={styles.btnTextTip}>10</Text></Button>
                                <Button bordered style={this.state.tipClicked2 ? styles.btnTipClicked : styles.btnTip} onPress={() => {this.tip(30)}}><Text style={styles.btnTextTip}>30</Text></Button>
                                <Button bordered style={this.state.tipClicked3 ? styles.btnTipClicked : styles.btnTip} onPress={() => {this.tip(50)}}><Text style={styles.btnTextTip}>50</Text></Button>
                                <Button bordered style={this.state.tipClicked4 ? styles.btnTipClicked : styles.btnTip} onPress={() => {this.tip(100)}}><Text style={styles.btnTextTip}>100</Text></Button>
                                <Button bordered style={this.state.tipClicked5 ? styles.btnTipClicked : styles.btnTip} onPress={() => {this.tip(200)}}><Text style={styles.btnTextTip}>200</Text></Button>
                            </View>
                        </View>

                    </CardItem>
                    <CardItem footer bordered style={styles.footer}>
                        <Button style={styles.btnOk}
                                onPress={() => {
                                    call()
                                }}>
                            <Text style={styles.text}>Ok</Text>
                        </Button>
                    </CardItem>

                </Card>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

    },
    cardbody: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        backgroundColor: 'rgba(0,0,0,0)',
        height: 300,
    },
    header: {
        height: 46,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: 'rgba(0,0,0,0.2)',
    },
    footer: {
        height: 45,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        backgroundColor: 'rgba(0,0,0,0)',
    },
    btnOk: {
        height: 46,
        backgroundColor: "rgba(0,0,0,0.2)",
        position: "absolute",
        right: 0,
        left: 0,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        zIndex: 100,
    },
    text: {
        color: "#fff",
        fontSize: 10,
    },
    modal: {
        position: "absolute",
        top: (DEVICE_HEIGHT / 2) - 180,
        width: 350,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
    },
    background: {
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
    },

    btnRate: {
        width: 50,
        height: 40,
    },

    btnTip: {
        width: 50,
        height: 40,
    },
    btnRateClicked: {
        width: 50,
        height: 40,
        backgroundColor: 'red'
    },

    btnTipClicked: {
        width: 50,
        height: 40,
        backgroundColor: 'red'
    },

    rateAndTip: {
        flex: 1,
        flexDirection: 'row',
        paddingTop:10,
        width: 350,
    },
    btnTextRate: {
        flex: 1,
        fontSize: 17,
        textAlign: 'center',
    },
    btnTextTip: {
        width: 60,
        textAlign: 'left',
        fontSize: 13,
    },


});
