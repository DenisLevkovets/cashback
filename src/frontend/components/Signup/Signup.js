import React from 'react';
import {
    ImageBackground,
    StyleSheet,
    Keyboard,
    Alert,
    TouchableOpacity,
    ScrollView,
    AsyncStorage,
    Modal,
    TextInput
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import {Container, Spinner, Content, Item, Input, Text, Button, View, Icon, Card, CardItem} from 'native-base';
import DatePicker from 'react-native-datepicker';
import bgScr from '../../images/wallpaper.png';
import Dimensions from 'Dimensions';
import Constants from '../../constants/Variables'
import SMS from './SMS'

const url = Constants.url;

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;


export default class Signup extends React.Component {


    constructor(props, context) {
        super(props, context);

        this.state = {
            DOB: "",
            name: '',
            email: '',
            pass: '',
            mobile: '',
            surname: '',
            city: '',
            refPhone: '',
            netInfo: false,
            loader: false,
            modalVisible: false,
            inputValue: "",
            SMSText: ""
        }


    }

    hideSMS(){
        this.setState({modalVisible:false})
    }

    setSMSText(value){
        this.setState({inputValue: value})
    }

    //Отправка смс
    sendSMS() {
        axios.post(url + '/sms', {
            number: this.state.mobile
        }).then(res => {
            if (res.status === 200) {
                if(res.data==="Можно отправлять только одно сообщение в день"){
                    alert(res.data)
                }
                else{
                    this.setState({modalVisible: true, SMSText: res.data})
                }
            }
        })
    }

    //Проверяем кол из смс
    checkSMS() {
        if (this.state.SMSText === this.state.inputValue) {
            this.createClient();
            this.setState({modalVisible: false});
        } else {
            alert("Неправильный код");
        }
    }

    //Отправка формы для регистрации
    createClient() {
        let ref = this.state.refPhone;
        axios.post(url + '/create', {
            name: this.state.name,
            surname: this.state.surname,
            city: this.state.city,
            dob: this.state.DOB,
            email: this.state.email,
            phone: this.state.mobile,
            pass: this.state.pass,
        }).then(resp => {

            //Обработка статуса ответа с сервера
            if (resp.status !== 200) {
                if (resp.status === 503) {
                    this.setState({loader: false});
                    Alert.alert("Network Error", "Проверьте подключение к интернету");
                } else {
                    Alert.alert("Ошибка");
                    this.setState({loader: false})
                }
            } else {
                let data = resp.status;
                if (data !== 200) {
                    this.setState({loader: false});
                    Alert.alert("Отказ", "Пользователь с этим именем уже зарегестрирован");
                } else {
                    Alert.alert("Поздравляю!", "Вы успешно зарегестрированы!");
                    AsyncStorage.setItem('phone', this.state.mobile);
                    //Проверяем пришёл ли клиент по приглашению
                    if (ref != "") {
                        axios.post(url + '/refBonus', {
                            phone: ref,
                            idCardOwn: resp.data.idCard
                        });
                    }
                    Actions.loginScreen();
                }
            }
        }).catch(function (error) {
            console.log(error);
        });
    };


    //Проверка правильности заполнения формы
    submit = async () => {
        if (this.state.DOB == "" || this.state.name == "" || this.state.surname == "" || this.state.email == "" || this.state.pass == "" || this.state.city == "" || this.state.mobile == "") {
            alert("Все поля должны быть заполнены");
        } else {
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            let regPass = /^([0-9]*)$/;
            let Value = this.state.pass.length.toString();
            let Value1 = this.state.mobile.length.toString();
            if (reg.test(this.state.email) === false) {
                alert("Неверный формат почты");
            } else if (regPass.test(this.state.pass) === false) {
                alert("Пароль должен содержать только цифры")
            } else if (Value != 5) {
                alert("Пин-код должен состоять из 5-ти символов")
            } else if (Value1 != 11) {
                alert("Номер должен содержать 11 символов")
            } else {
                Keyboard.dismiss();
                this.sendSMS();
            }
        }
    };

    render() {
        if (this.state.loader == false) {
            return (
                <ScrollView style={styles.container}>
                    <ImageBackground source={bgScr} style={styles.background}>
                        <View style={styles.header}>
                            <TouchableOpacity onPress={() => {
                                Actions.loginScreen();
                            }}>
                                <Icon ios="ios-arrow-back" android="md-arrow-back" style={{width: 30, height: 30}}/>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.content}>
                            <Container style={{flex: 1, alignSelf: 'center', marginTop: 20,}}>
                                <Content>

                                    <Item rounded style={styles.item}>
                                        <Input placeholder='Имя'
                                               onChangeText={(name) => this.setState({name})} value={this.state.name}/>
                                    </Item>

                                    <Item rounded style={styles.itemMargin}>
                                        <Input placeholder='Фамилия'
                                               onChangeText={(surname) => this.setState({surname})}
                                               value={this.state.surname}/>
                                    </Item>

                                    <Item rounded style={styles.itemMargin}>
                                        <Input type="password" placeholder='Пин-код из 5-ти символов'
                                               secureTextEntry={true} keyboardType='phone-pad'
                                               onChangeText={(pass) => this.setState({pass})} value={this.state.pass}/>
                                    </Item>

                                    <Item rounded style={styles.itemMargin}>
                                        <Input placeholder='Email' keyBoardType="email-address"
                                               onChangeText={(email) => this.setState({email})}
                                               value={this.state.email}/>
                                    </Item>

                                    <Item rounded style={styles.itemMargin}>
                                        <Input placeholder='Телефон 8-ХХХ-ХХХ-ХХ-ХХ' keyboardType='phone-pad'
                                               onChangeText={(mobile) => this.setState({mobile})}
                                               value={this.state.mobile}/>
                                    </Item>
                                    <View>
                                        <DatePicker
                                            style={styles.datePicker}
                                            date={this.state.DOB}
                                            mode="date"
                                            placeholder="Дата рождения"
                                            androidMode="spinner"
                                            format="YYYY-MM-DD"
                                            minDate="1950-01-01"
                                            maxDate="2019-01-31"
                                            confirmBtnText="OK"
                                            cancelBtnText="Закрыть"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'relative',
                                                    left: 0,
                                                    top: 2,
                                                    marginLeft: 0,
                                                },
                                                dateInput: {
                                                    marginLeft: -110,
                                                    borderWidth: 0,
                                                    borderBottomWidth: 1,
                                                },
                                                dateText: {
                                                    textAlign: 'left',
                                                    fontSize: 17,
                                                    color: 'black',
                                                },
                                                placeholderText: {
                                                    textAlign: 'left',
                                                    fontSize: 18,
                                                    color: 'grey',
                                                }
                                            }}
                                            onDateChange={(DOB) => {
                                                this.setState({DOB: DOB})
                                            }}/>
                                    </View>


                                    <Item rounded style={styles.itemMargin}>
                                        <Input placeholder='Город' onChangeText={(city) => this.setState({city})}
                                               value={this.state.city}/>
                                    </Item>

                                    <Item rounded style={styles.itemMargin}>
                                        <Input placeholder='Введите тел номер,кто вас пригласил'
                                               onChangeText={(refPhone) => this.setState({refPhone})}
                                               value={this.state.refPhone} keyboardType='phone-pad'/>
                                    </Item>

                                    <Button rounded style={{alignSelf: 'center', marginTop: 15}} onPress={this.submit}>
                                        <Text>Зарегистрироваться</Text>
                                    </Button>

                                </Content>
                            </Container>
                        </View>


                        <Modal transparent={true}
                               visible={this.state.modalVisible}
                               onRequestClose={() => {
                               }}
                        >
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: 'rgba(0,0,0,0.5)',
                            }}>
                                <SMS checkSMS={this.checkSMS.bind(this)} hideSMS={this.hideSMS.bind(this)} setSMSText={this.setSMSText.bind(this)}/>
                            </View>
                        </Modal>


                    </ImageBackground>
                </ScrollView>
            );
        }

        else {
            return (
                <View style={styles.container}>

                    <Spinner color='red' size={60}/>
                </View>
            );
        }
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "flex-start",
        height: 45,
        width: DEVICE_WIDTH,
        paddingTop: 60,
        paddingLeft: 15,
    },
    background: {
        flex: 1,
        alignSelf: 'stretch',
        width: null,
        justifyContent: 'center',
        alignItems: 'center'

    },
    content: {
        paddingTop: 20,
        alignItems: 'center',
    },
    datePicker: {
        width: null,
        marginTop: 15,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        borderColor: 'rgba(255, 255, 255, 0.5)',
        borderRadius: 20,
        overflow: 'hidden'
    },
    item: {
        width: 300,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        borderColor: 'rgba(255, 255, 255, 0.5)'
    },
    itemMargin: {
        width: 300,
        marginTop: 15,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        borderColor: 'rgba(255, 255, 255, 0.5)'
    },

});
