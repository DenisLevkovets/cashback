import React, {Component} from 'react';
import Dimensions from 'Dimensions';
import {StyleSheet, View, Text, Modal, AsyncStorage} from 'react-native';
import {Actions} from 'react-native-router-flux';
import SMS from "../Signup/SMS";
import Phone from "./Phone"
import Constants from '../../constants/Variables';
import axios from "axios";

const url = Constants.url;

export default class SignupSection extends Component {
    constructor() {
        super();
        this.state = {
            smsVisible: false,
            phoneVisible: false,
            phone: "",
            SMSText: "",
            inputValue: "",
        };
    }

    hidePhone() {
        this.setState({phoneVisible: false})
    }

    showPhone() {
        this.setState({phoneVisible: true})
    }

    setPhone(number) {
        this.setState({phone: number});
    }

    setSMSText(text) {
        this.setState({inputValue: text});
    }

    hideSMS() {
        this.setState({smsVisible: false})
    }

    //Отправка смс
    sendSMS() {
        if (this.state.phone.length !== 11) {
            alert("Номер должен содержать 11 символов")
        }
        else {
            axios.post(url + '/sms', {
                number: this.state.phone
            }).then(res => {
                if (res.status === 200) {
                    if (res.data === "Можно отправлять только одно сообщение в день") {
                        alert(res.data)
                    }
                    else {
                        this.hidePhone();
                        this.setState({smsVisible: true, SMSText: res.data})
                    }
                }
            })
        }
    }

    //Проверяем кол из смс
    checkSMS() {
        console.log(this.state.SMSText);
        console.log(this.state.inputValue);
        if (this.state.SMSText === this.state.inputValue) {
            AsyncStorage.setItem('phone',this.state.phone);
            this.setState({smsVisible: false});
        } else {
            alert("Неправильный код");
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text} onPress={() => Actions.Signup()}>Зарегистрироваться</Text>
                <Text style={styles.text} onPress={() => this.showPhone()}>Восстановить доступ</Text>
                <Modal transparent={true}
                       visible={this.state.phoneVisible}
                       onRequestClose={() => {
                       }}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)',
                    }}>
                        <Phone sendSMS={this.sendSMS.bind(this)} hidePhone={this.hidePhone.bind(this)}
                               setPhone={this.setPhone.bind(this)}/>
                    </View>
                </Modal>

                <Modal transparent={true}
                       visible={this.state.smsVisible}
                       onRequestClose={() => {
                       }}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)',
                    }}>
                        <SMS checkSMS={this.checkSMS.bind(this)} hideSMS={this.hideSMS.bind(this)}
                             setSMSText={this.setSMSText.bind(this)}/>
                    </View>
                </Modal>
            </View>
        );
    }
}
const DEVICE_WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        top: 65,
        width: DEVICE_WIDTH,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    text: {
        color: 'white',
        backgroundColor: 'transparent',
    },
});


