import React from 'react';
import Logo from './Logo';
import Form from './Form';
import Wallpaper from './Wallpaper';
import SignupSection from './SignupSection';

export default class LoginScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };


    //Объединяющий компонент
    render() {
        return (
            <Wallpaper>
                <Logo/>
                <Form navigation={this.props.navigation}/>
                <SignupSection/>
            </Wallpaper>
        );
    }
}
