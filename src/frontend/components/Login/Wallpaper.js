import React, {Component} from 'react';
import Dimensions from 'Dimensions';
import {StyleSheet, ImageBackground} from 'react-native';

import bgSrc from '../../images/wallpaper.png';

export default class Wallpaper extends Component {
  //Ставим бэкгрануд для любых компонентов внутри
  render() {
    return (
      <ImageBackground style={styles.picture} source={bgSrc}>
        {this.props.children}
      </ImageBackground>
    );
  }
}
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  picture: {
    flex: 1,
    height: DEVICE_HEIGHT,
    resizeMode: 'cover',
  },
});
