import React, {Component} from 'react';
import Dimensions from 'Dimensions';
import {
    Alert,
    StyleSheet,
    KeyboardAvoidingView,
    TouchableOpacity,
    Image, Animated, Text, Easing,
    AsyncStorage
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Constants from '../../constants/Variables'
import {registerForPushNotificationsAsync} from '../Push'
import passwordImg from '../../images/password.png';
import eyeImg from '../../images/eye_black.png';
import {Input, Item} from "native-base";
import spinner from "../../images/loading.gif";
import axios from "axios";

const url = Constants.url;

const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 40;
export default class Form extends Component {
    constructor() {
        super();
        this.state = {
            showPass: true,
            press: false,
            isLoading: false,
            passlog: "",
        };
        this.showPass = this.showPass.bind(this);
        this.buttonAnimated = new Animated.Value(0);
        this.growAnimated = new Animated.Value(0);
        this._onPress = this._onPress.bind(this);
    }

    //Анимация прогрузки и вызов login
    _onPress() {
        var prop = this.props.navigation;
        if (this.state.isLoading) return;
        this.setState({isLoading: true});
        Animated.timing(this.buttonAnimated, {
            toValue: 1,
            duration: 200,
            easing: Easing.linear,
        }).start();

        setTimeout(() => {
            this._onGrow();
        }, 2000);

        setTimeout(() => {

            this.login(prop);
            this.setState({isLoading: false});
            this.buttonAnimated.setValue(0);
            this.growAnimated.setValue(0);
        }, 2300);
    }

    //Продолжение анимации
    _onGrow() {
        Animated.timing(this.growAnimated, {
            toValue: 1,
            duration: 200,
            easing: Easing.linear,
        }).start();
    }

    //Проверка введённых данных и помещение нужных в локальное хранилище
    async login(prop) {
        axios.post(url+'/login', {
            phone: await AsyncStorage.getItem('phone'),
            pass: this.state.passlog
        }).then(function (resp) {
            if (resp.data.status === "worker") {
                AsyncStorage.setItem("card", resp.data.idC.toString());
                AsyncStorage.setItem("client", resp.data.id.toString());
                AsyncStorage.setItem("worker", resp.data.idW.toString());
                registerForPushNotificationsAsync();
                prop.navigate('Worker');
            }
            else if (resp.data.status === "client") {
                AsyncStorage.setItem("card", resp.data.idC.toString());
                AsyncStorage.setItem("client", resp.data.id.toString());
                AsyncStorage.setItem("worker", resp.data.idW.toString());
                registerForPushNotificationsAsync();
                prop.navigate('Main');
            }
            else {
                Alert.alert("Отказ", "Неверный логин или пароль")
            }
        }).catch(function (error) {
            console.log(error);
        });
    }

    //Показ введённого пароля
    showPass() {
        this.state.press === false
            ? this.setState({showPass: false, press: true})
            : this.setState({showPass: true, press: false});
    }

    render() {
        //Часть анимации
        const changeWidth = this.buttonAnimated.interpolate({
            inputRange: [0, 1],
            outputRange: [DEVICE_WIDTH - MARGIN, MARGIN],
        });
        const changeScale = this.growAnimated.interpolate({
            inputRange: [0, 1],
            outputRange: [1, MARGIN],
        });

        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <Item rounded style={{
                    width: 350,
                    backgroundColor: 'rgba(255, 255, 255, 0.5)',
                    borderColor: 'rgba(255, 255, 255, 0.5)'
                }}>
                    <Image source={passwordImg} style={styles.inlineImg}/>
                    <Input
                        secureTextEntry={this.state.showPass}
                        placeholder="Пароль" keyboardType='number-pad'
                        onChangeText={(passlog) => this.setState({passlog})} value={this.state.passlog}
                    />

                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={styles.btnEye}
                        onPress={this.showPass}>
                        <Image source={eyeImg} style={styles.iconEye}/>
                    </TouchableOpacity>
                </Item>


                <Animated.View style={{width: changeWidth, paddingTop: 30}}>
                    <TouchableOpacity
                        style={styles.button1}
                        onPress={this._onPress}
                        activeOpacity={1}>
                        {this.state.isLoading ? (
                            <Image source={spinner} style={styles.image1}/>
                        ) : (
                            <Text style={styles.text1}>LOGIN</Text>
                        )}
                    </TouchableOpacity>
                    <Animated.View
                        style={[styles.circle1, {transform: [{scale: changeScale}]}]}
                    />
                </Animated.View>

            </KeyboardAvoidingView>

        );
    }
}


const styles = StyleSheet.create({
    container: {

        alignItems: 'center',
    },
    btnEye: {
        marginRight: 10
    },
    iconEye: {
        width: 25,
        height: 25,
        tintColor: 'rgba(0,0,0,0.2)',
    },
    input: {
        width: 300,
        backgroundColor: 'rgba(255, 255, 255, 0.5)'
    },
    button1: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F035E0',
        height: MARGIN,
        borderRadius: 20,
        zIndex: 1000,
    },
    circle1: {
        height: MARGIN,
        width: MARGIN,
        marginTop: -MARGIN,
        borderWidth: 1,
        borderColor: '#F035E0',
        borderRadius: 100,
        alignSelf: 'center',
        zIndex: 100,
        backgroundColor: '#F035E0',
    },
    text1: {
        color: 'white',
        backgroundColor: 'transparent',
    },
    image1: {
        width: 24,
        height: 24,
    },
    inlineImg: {
        width: 30,
        height: 30,
        marginLeft: 10,
    }
});
