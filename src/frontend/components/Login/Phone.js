import React from 'react';
import {StyleSheet, View, TextInput} from 'react-native';
import {Card, CardItem, Text, Button} from 'native-base';
import Dimensions from 'Dimensions';

const DEVICE_HEIGHT = Dimensions.get('window').height;


export default class Phone extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.card}>
                <Card style={styles.modal}>
                    <CardItem bordered style={styles.cardbody}>
                        <TextInput
                            style={styles.TextInputStyle}
                            onChangeText={number => this.props.setPhone(number)}
                            underlineColorAndroid="transparent"
                            placeholder="Введите номер телефона "
                            keyboardType='number-pad'
                        />
                    </CardItem>

                    <CardItem footer bordered style={styles.footer}>
                        <Button style={styles.btnCancel}
                                onPress={() => {
                                    this.props.hidePhone()
                                }}>
                            <Text style={styles.text}>Cancel</Text>
                        </Button>
                        <Button style={styles.btnOk}
                                onPress={() => {
                                    this.props.sendSMS();
                                }}>
                            <Text style={styles.text}>Ok</Text>
                        </Button>
                    </CardItem>
                </Card>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    btnCancel: {
        height: 46,
        backgroundColor: "#000",
        position: "absolute",
        left: 0,
        width: 150,
        borderBottomLeftRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnOk: {
        height: 46,
        backgroundColor: "#000",
        position: "absolute",
        right: 0,
        width: 150,
        borderBottomRightRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
    },
    TextInputStyle: {
        height: 40,
        width: 180,
        marginTop: 10,
        borderWidth: 1,
        textAlign: 'center',
    },
    card: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardbody: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: 150,
    },
    footer: {
        height: 45,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },
    modal: {
        position: "absolute",
        top: DEVICE_HEIGHT / 2 - 200,
        width: 300,
        borderColor: 'rgba(0,0,0,0)',
        backgroundColor: 'rgba(0,0,0,0)',

    },
    text: {
        color: "#fff",
        fontSize: 10,
    },


});
