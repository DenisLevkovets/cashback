import React from 'react';
import {StyleSheet, View, TextInput, AsyncStorage, Alert} from 'react-native';
import {Card, CardItem, Text, Button} from 'native-base'
import axios from "axios";
import Constants from '../../constants/Variables'

const url = Constants.url;

export default class QR extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputValue: '',
            idCompany: 0,
            CompanyName: "",
            Industry: ""
        };
    }

    componentDidMount() {
        this.getCompanyInfo();
    }

    async getCompanyInfo() {
        axios.post(url + '/companyInfo', {
            idW: await AsyncStorage.getItem('worker')
        }).then(res => {
            if (res.status === 200) {
                this.setState({
                    idCompany: res.data[0].idCompany,
                    CompanyName: res.data[0].CompanyName,
                    Industry: res.data[0].Industry
                })
            }
        })
    }

    checkPush(){
        let data = this.props.qrdata.split('---');
        axios.post(url + '/checkPush', {
            idmessage: data[12]
        }).then(resp=>{
            if(resp.data[0].isCanceled===0){
                this.createDeal();
            }
            else{
                Alert.alert('Ошибка', 'Вы уже использовали этот бонус');
            }
        })
    }

    //Запрос создания сделки
    async createDeal() {
        //Парсим данные,которые считали с ReadQR
        let data = this.props.qrdata.split('---');
        axios.post(url + '/deal', {
            value: data[0],
            clientbonus: data[1],
            usedbonus: data[2],
            company: data[11] === 'Обычная' ? this.state.CompanyName : data[3],
            industry: data[11] === 'Обычная' ? this.state.Industry : data[4],
            city: data[5],
            idClient: data[6],
            workerbonus: data[7],
            systembonus: data[8],
            idCard: data[9],
            idCompany: data[11] === 'Обычная' ? this.state.idCompany : data[10],
            sum: this.state.inputValue,
            idWorker: await AsyncStorage.getItem('worker'),
            idWorkerCard: await AsyncStorage.getItem('card'),
            status: data[11],
            idmessage: data[11] === 'Пуш' ? data[12] : 0
        }).then(res => {
            if (res.status === 200) {
                Alert.alert("Успех", "Покупка успешно совершена")
            }
        });
    }


    render() {
        let hideConfirm = this.props.hideConfirm;
        let qrdata = this.props.qrdata.split('---');
        return (
            <View style={styles.card}>
                <Card style={styles.modal}>
                    <CardItem bordered style={styles.cardbody}>
                        <Text>{qrdata[4]} {qrdata[3]} - {qrdata[5]}</Text>
                        <Text>Скидка -{qrdata[0]}%</Text>
                        <TextInput
                            style={styles.TextInputStyle}
                            onChangeText={text => this.setState({inputValue: text})}
                            underlineColorAndroid="transparent"
                            placeholder="Введите сумму покупки"
                        />
                    </CardItem>
                    <CardItem footer bordered style={styles.footer}>
                        <Button style={styles.btnCancel}
                                onPress={() => {
                                    hideConfirm()
                                }}>
                            <Text style={styles.text}>Cancel</Text>
                        </Button>
                        <Button style={styles.btnOk}
                                onPress={() => {
                                    if(qrdata[11]==='Пуш') this.checkPush();
                                    else this.createDeal();
                                    hideConfirm();
                                }}>
                            <Text style={styles.text}>Ok</Text>
                        </Button>
                    </CardItem>
                </Card>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardbody: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: 200,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },

    footer: {
        height: 45,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },
    btnCancel: {
        height: 46,
        backgroundColor: "#000",
        position: "absolute",
        left: 0,
        width: 150,
        borderBottomLeftRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnOk: {
        height: 46,
        backgroundColor: "#000",
        position: "absolute",
        right: 0,
        width: 150,
        borderBottomRightRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: "#fff",
        fontSize: 10,
    },
    modal: {
        position: "absolute",
        top: 150,
        width: 300,
        borderColor: 'rgba(0,0,0,0)',
        backgroundColor: 'rgba(0,0,0,0)',

    },
    TextInputStyle: {
        height: 40,
        width: 180,
        marginTop: 10,
        borderWidth: 1,
        textAlign: 'center',
    },


});
