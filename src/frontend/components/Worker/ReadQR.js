import React from 'react';
import {StyleSheet, View, Button, Modal, Text, AsyncStorage, Alert, TouchableOpacity} from 'react-native';
import {BarCodeScanner, Permissions} from 'expo';
import Confirm from "../Worker/Confirm";
import Dimensions from 'Dimensions';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;


export default class ReadQR extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hasCameraPermission: null,
            modalVisible: false,
            confirmVisible: false,
            qrdata: "",
        };
    }



    //Проверяем не покупает ли кассир сам у себя,иначе выводим экран подтверждения покупки
    showAlert = async (data) => {
        {
            this.setState({modalVisible: false, qrdata: data});
            if (data.split('---')[9] == await AsyncStorage.getItem('card')) {
                Alert.alert("Ошибка", "Вы не можете покупать сами у себя");
            }
            else {
                this.setState({confirmVisible: true})
            }
        }
    };

    hideConfirm() {
        this.setState({confirmVisible: false});
    }

    //Выполняется в первую очередь
    async componentDidMount() {
        const {status} = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({hasCameraPermission: status === 'granted'});
    }

    render() {

        const {hasCameraPermission} = this.state;

        if (hasCameraPermission === null) {
            return <Text>Requesting for camera permission</Text>;
        }
        if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        }
        return (
            <View style={styles.container}>
                <Button title={"ReadQR"} onPress={() => {
                    this.setState({modalVisible: true});
                }}/>

                <Modal transparent={true}
                       visible={this.state.modalVisible}
                       onRequestClose={() => {
                           this.setState({modalVisible: false});
                       }}>

                    <View style={{flex: 1}}>
                        <BarCodeScanner
                            onBarCodeScanned={(type, data) => {
                                this.showAlert(type.data);
                            }}
                            style={styles.reader}
                        />
                        <TouchableOpacity
                            style={styles.loginScreenButton}
                            onPress={() => {
                                this.setState({modalVisible: false})
                            }}
                            underlayColor='#fff'>
                            <Text style={styles.loginText}>Закрыть</Text>
                        </TouchableOpacity>

                    </View>
                </Modal>

                <Modal transparent={true}
                       visible={this.state.confirmVisible}
                       onRequestClose={() => {
                           this.setState({confirmVisible: false});
                       }}
                       onBackdropPress={() => this.setState({confirmVisible: false})}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)',
                    }}>
                        <Confirm hideConfirm={this.hideConfirm.bind(this)} qrdata={this.state.qrdata}/>
                    </View>
                </Modal>
            </View>
        );
    }
}

const
    styles = StyleSheet.create({
        container: {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
        reader: {
            flex: 1,
            width: DEVICE_WIDTH,
            height: DEVICE_HEIGHT - 40,
        },
        loginScreenButton: {
            height:40,
            backgroundColor: '#1E6738',
            width: DEVICE_WIDTH,
        },
        loginText: {
            color: '#fff',
            textAlign: 'center',
            height:40,
            fontSize:17,
            width:DEVICE_WIDTH,
            paddingLeft: 20,
            paddingTop: 5,
        }
    });
