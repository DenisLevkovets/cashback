import React from 'react';
import {Platform} from 'react-native';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';

import TabBarIcon from '../TabBarIcon';
import HistoryScreen from '../../screens/HistoryScreen';
import OfferScreen from '../../screens/OfferScreen';
import ProfileScreen from '../../screens/ProfileScreen';
import IndustryScreen from '../../screens/IndustryScreen';

//Устанавливаем иконки и сами страницы

const ProfileStack = createStackNavigator({
    Profile: ProfileScreen,
});

ProfileStack.navigationOptions = {
    tabBarLabel: 'Профиль',
    tabBarIcon: ({focused}) => (
        <TabBarIcon
            focused={focused}
            name={
                Platform.OS === 'ios'
                    ? `ios-home`
                    : 'md-home'
            }
        />
    ),
};

const OfferStack = createStackNavigator({
    Offer: OfferScreen,
});

OfferStack.navigationOptions = {
    tabBarLabel: 'Предложения',
    tabBarIcon: ({focused}) => (
        <TabBarIcon
            focused={focused}
            name={Platform.OS === 'ios' ? 'ios-cart' : 'md-cart'}
        />
    ),
};

const IndustryStack = createStackNavigator({
    Industry: IndustryScreen,
});

IndustryStack.navigationOptions = {
    tabBarLabel: 'Отрасли',
    tabBarIcon: ({focused}) => (
        <TabBarIcon
            focused={focused}
            name={Platform.OS === 'ios' ? 'ios-build' : 'md-build'}
        />
    ),
};


const HistoryStack = createStackNavigator({
    History: HistoryScreen,
});

HistoryStack.navigationOptions = {
    tabBarLabel: 'История',
    tabBarIcon: ({focused}) => (
        <TabBarIcon
            focused={focused}
            name={Platform.OS === 'ios' ? 'ios-list' : 'md-list'}
        />
    ),
};




//Определяем порядок
export default createBottomTabNavigator({
    ProfileStack,
    OfferStack,
    IndustryStack,
    HistoryStack,
});
