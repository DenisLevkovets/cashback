import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import WorkerTabNavigator from './WorkerTabNavigator';
import LoginScren from "../../screens/LoginScren";

export default createAppContainer(createSwitchNavigator({
  // Указываем основные разделы навигации приложения

  //Навигация логина и регистрации
  Login: LoginScren,
  //Навигация внутри приложения для клиента
  Main: MainTabNavigator,
  //Навигация внутри приложения для работника
  Worker: WorkerTabNavigator,
}));