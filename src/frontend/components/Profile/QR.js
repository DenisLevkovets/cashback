import React from 'react';
import {StyleSheet, View, TouchableOpacity, TextInput, AsyncStorage, Alert} from 'react-native';
import {Card, CardItem, Text, Button} from 'native-base'
import QRCode from 'react-native-qrcode';

export default class QR extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            inputValue: '',
            valueForQRCode: '',
        };
    }

    componentDidMount(){
        this.setState({inputValue:this.props.defaultBonus.toString()})
    }

    //Собираем данные в строку,чтобы сгенерировать её в qr код
    getTextInputValue = async () => {
        if (parseInt(this.props.defaultBonus)<parseInt(this.state.inputValue)){
            Alert.alert("Ошибка","Недостаточно баллов");
        }
        else if(parseInt(this.props.defaultBonus<0)){
            Alert.alert("Ошибка","Нельзая ввести отрицательное число");
        }
        else {
            let offer = this.props.offer;
            let push = this.props.push;
            let str = "";
            if (offer.length !== 0) {
                let value = offer[2];
                let clientbonus = offer[4];
                let usedbonus = this.state.inputValue;
                let companyname = offer[6];
                let industry = offer[7];
                let city = offer[3];
                let workerbonus = offer[8];
                let systembonus = offer[9];
                let idcompany = offer[10];
                str = value + "---" + clientbonus + "---" + usedbonus + "---" + companyname + "---" + industry + "---" + city +
                    "---" + await AsyncStorage.getItem('client') + "---" + workerbonus + "---" + systembonus + "---" + await AsyncStorage.getItem('card') + "---" + idcompany+"---"+"Предложение";

            } else if(push.length!==0) {
                let value = push[3];
                let clientbonus = 0;
                let usedbonus = this.state.inputValue;
                let companyname = push[1];
                let industry = push[0];
                let city = this.props.city;
                let workerbonus = 0;
                let systembonus = 0;
                let idcompany = push[5];
                let idmessage = push[6];
                console.log('------'+idmessage);
                str = value+ "---" + clientbonus + "---" + usedbonus + "---" + companyname + "---" + industry + "---" + city +
                    "---" + await AsyncStorage.getItem('client') + "---" + workerbonus + "---" + systembonus + "---" + await AsyncStorage.getItem('card') + "---" + idcompany+"---"+"Пуш"+"---"+idmessage;
            }else{
                let value = 0;
                let clientbonus = 0;
                let usedbonus = this.state.inputValue;
                let companyname = "companyname";
                let industry = "industry";
                let city = this.props.city;
                let workerbonus = 0;
                let systembonus = 0;
                let idcompany = 0;
                str = value+ "---" + clientbonus + "---" + usedbonus + "---" + companyname + "---" + industry + "---" + city +
                    "---" + await AsyncStorage.getItem('client') + "---" + workerbonus + "---" + systembonus + "---" + await AsyncStorage.getItem('card') + "---" + idcompany+"---"+"Обычная";
            }
            this.setState({valueForQRCode: str});
        }
    };

    render() {
        let hideqr = this.props.hideqr;
        return (
            <View style={styles.card}>
                <Card style={styles.modal}>

                    <CardItem bordered style={styles.cardbody}>
                        <QRCode
                            value={this.state.valueForQRCode}
                            size={150}
                            bgColor="#000"
                            fgColor="#fff"
                        />
                        <TextInput
                            style={styles.TextInputStyle}
                            onChangeText={text => this.setState({inputValue: text})}
                            underlineColorAndroid="transparent"
                            placeholder="Введите количество баллов"
                            value={this.state.inputValue}
                        />
                        <TouchableOpacity
                            onPress={this.getTextInputValue}
                            activeOpacity={0.7}
                            style={styles.button}>
                            <Text style={styles.TextStyle}> Сгенерировать QR </Text>
                        </TouchableOpacity>
                    </CardItem>
                    <CardItem footer bordered style={styles.footer}>
                        <Button style={styles.btnCancel}
                                onPress={() => {
                                    hideqr()
                                }}>
                            <Text style={styles.text}>Cancel</Text>
                        </Button>
                    </CardItem>
                </Card>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardbody: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: 400,
    },
    header: {
        height: 46,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },
    footer: {
        height: 45,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },
    btnCancel: {
        height: 46,
        backgroundColor: "#000",
        position: "absolute",
        right: 0,
        left: 0,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8
    },
    text: {
        color: "#fff",
        fontSize: 10,
    },
    modal: {
        position: "absolute",
        top: 50,
        width: 300,
        borderColor: 'rgba(0,0,0,0)',
        backgroundColor: 'rgba(0,0,0,0)',

    },
    TextInputStyle: {
        height: 40,
        width:180,
        marginTop: 10,
        borderWidth: 1,
        textAlign: 'center',
    },

    button: {
        paddingTop: 8,
        marginTop: 10,
        paddingBottom: 8,
        backgroundColor: '#F44336',
        marginBottom: 20,
    },

    TextStyle: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 18,
    },


});
