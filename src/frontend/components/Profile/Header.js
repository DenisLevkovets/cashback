import React from 'react';
import {StyleSheet, Text, View, Platform, TouchableOpacity, ActionSheetIOS} from 'react-native';
import {Picker, Icon} from 'native-base'
import Dimensions from 'Dimensions';


const DEVICE_WIDTH = Dimensions.get('window').width;
const items = [
    'Астрахань', 'Абакан', 'Анапа', 'Азов', 'Ангарск',
    'Барнаул', 'Белгород', 'Батайск', 'Брянск', 'Бор',
    'Воронеж', 'Вологда', 'Волгоград', 'Владивосток', 'Владимир',
    'Гатчина', 'Георгиевск', 'Грозный', 'Губкин', 'Гуково',
    'Дзержинск', 'Дмитров', 'Долгопрудный', 'Домодедово', 'Дубна',
    'Евпатория', 'Ейск', 'Екатеринбург', 'Елец', 'Ессентуки',
    'Железногорск', 'Жигулевск', 'Жуковский',
    'Заречный', 'Зеленогорск', 'Зеленодольск', 'Златоуст',
    'Иваново', 'Ижевск', 'Иркутск', 'Ишим', 'Ишимбай',
    'Йошкар-Ола',
    'Казань', 'Калининград', 'Калуга', 'Когалым', 'Кострома',
    'Лениногорск', 'Лесосибирск', 'Липецк', 'Лиски', 'Люберцы',
    'Магадан', 'Магнитогорск', 'Махачкала', 'Москва', 'Мурманск',
    'Нефтеюганск', 'Нижневартовск', 'Новосибирск', 'Норильск', 'Ноябрьск',
    'Октябрьский', 'Омск', 'Орел', 'Оренбург', 'Орск',
    'Пенза', 'Пермь', 'Петрозаводск', 'Прокопьевск', 'Псков',
    'Ревда', 'Ржев', 'Рубцовск', 'Рыбинск', 'Рязань',
    'Самара', 'Севастополь', 'Смоленск', 'Сочи', 'Ставрополь',
    'Тобольск', 'Тверь', 'Томск', 'Тула', 'Тюмень',
    'Узловая', 'Ульяновск', 'Уссурийск', 'Уфа',
    'Феодосия', 'Фрязино',
    'Хабаровск', 'Ханты-Мансийск', 'Хасавюрт', 'Химки',
    'Чебоксары', 'Челябинск', 'Череповец', 'Черкесск', 'Черногорск',
    'Шадринск', 'Шали', 'Шахты', 'Шуя',
    'Щекино', 'Щелково',
    'Электросталь', 'Элиста', 'Энгельс',
    'Южно-Сахалинск', 'Юрга',
    'Якутск', 'Ялта', 'Ярославль', 'Иннополис'
];

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            city: "",
        };
    }


    showActionSheet = () => {
        let setTempCity = this.props.setTempCity;
        let refresh = this.props.refresh;
        ActionSheetIOS.showActionSheetWithOptions({
                options: items,
                cancelButtonIndex: items.length + 1,
                destructiveButtonIndex: items.length,
            },
            (buttonIndex) => {
                this.setState({city: items[buttonIndex]});
                setTempCity(items[buttonIndex]);
                refresh(items[buttonIndex]);
            });
    };

    render() {
        //Шапка для IOS
        if (Platform.OS === 'ios') {
            //Методы из родительского класса
            let showqr = this.props.showqr;
            return (
                <View style={styles.container}>
                    <View style={styles.first}>
                        <TouchableOpacity onPress={() => {
                            showqr();
                        }}>
                            <Icon name="qr-scanner" style={{width: 25}}/>
                        </TouchableOpacity>

                        <Text style={styles.name}>{this.props.clientData[0]}</Text>

                        <TouchableOpacity onPress={() => {
                            this.props.showSettings();
                        }}>
                            <Icon ios='ios-brush' android='md-brush' style={{width: 25}}/>
                        </TouchableOpacity>
                    </View>

                    <Text onPress={this.showActionSheet} style={styles.city}>
                        {this.state.city==="" ? (this.props.clientData.length > 0? (this.props.clientData[2]==null?this.props.clientData[1]:this.props.clientData[2]):"-") : this.state.city}
                    </Text>
                    <Text style={styles.rating}>Рейтинг</Text>
                    <View style={styles.balanceView}>
                        <Text style={styles.balance}>Баланс - {this.props.balance} баллов</Text>
                    </View>
                </View>

            );
        }
        //Шапка для Android
        else {
            //Методы из родительского класса
            let showqr = this.props.showqr;
            let setTempCity = this.props.setTempCity;
            let refresh = this.props.refresh;
            return (
                <View style={styles.container}>
                    <View style={styles.first}>
                        <TouchableOpacity onPress={() => {
                            showqr();
                        }}>
                            <Icon name="qr-scanner" style={{width: 25}}/>
                        </TouchableOpacity>

                        <Text style={styles.name}>{this.props.clientData[0]}</Text>

                        <TouchableOpacity onPress={() => {
                            this.props.showSettings();
                        }}>
                            <Icon ios='ios-brush' android='md-brush' style={{width: 25}}/>
                        </TouchableOpacity>
                    </View>

                    <Picker
                        selectedValue={this.state.city}
                        style={styles.picker}
                        isVisible={this.state.isVisible}
                        onClick={(item) => {
                            this.setState({city: item});
                        }}
                        onClose={() => this.setState({isVisible: false})}
                        onValueChange={(itemValue, itemIndex) => {
                            this.setState({city: itemValue});
                            setTempCity(itemValue);
                            refresh(itemValue);
                        }}>
                        <Picker.Item label={this.props.clientData.length > 0 ? (this.props.clientData[2]==null?this.props.clientData[1]:this.props.clientData[2]) : "-"}
                                     value={this.props.clientData.length > 0 ? (this.props.clientData[2]==null?this.props.clientData[1]:this.props.clientData[2]) : "-"} key={0}/>
                        {items.map((item, index) => {
                            return (<Picker.Item label={item} value={item} key={index + 1}/>)
                        })}
                    </Picker>
                    <Text style={styles.rating}>Рейтинг</Text>
                    <View style={styles.balanceView}>
                        <Text style={styles.balance}>Баланс - {this.props.balance} баллов</Text>
                    </View>


                </View>

            );
        }

    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 30,
        flex: 1,
        alignItems: 'flex-start',
        backgroundColor: 'rgba(255,245,70,0)',
    },
    name: {
        fontSize: 17,
        width: DEVICE_WIDTH - 135,
        marginLeft: 10,
    },
    first: {
        marginLeft: 65,
        height: 20,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    picker: {
        height: 50,
        marginTop: 10,
        width: 220,
        marginLeft: 92,
    },
    city: {
        marginTop: 15,
        width: 220,
        marginLeft: 100,
        fontSize: 17
    },
    rating: {
        fontSize: 17,
        marginTop: 10,
        width: DEVICE_WIDTH,
        marginLeft: 100,
    },
    balanceView: {
        marginTop: 50,
        marginLeft: 10,
    },
    balance: {
        fontSize: 17,
    },

});
