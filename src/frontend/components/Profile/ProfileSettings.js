import React from 'react';
import {StyleSheet, TouchableOpacity, View, Text, ScrollView, Keyboard, AsyncStorage} from 'react-native';
import {Icon, Input, Item} from "native-base";
import DatePicker from "react-native-datepicker";
import Dimensions from 'Dimensions';
import axios from "axios";
import Constants from '../../constants/Variables'

const url = Constants.url;
const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;


export default class ProfileSettings extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            email: "",
            mobile: "",
            DOB: "",
            city: ""
        };
    }

    componentDidMount() {
        let clientData = this.props.clientData;
        this.setState({
            name: clientData[0],
            email: clientData[4],
            mobile: clientData[5],
            DOB: clientData[3],
            city: clientData[1]
        });
    }

    async acceptChanges() {
        axios.post(url + '/updateClient', {
            id: await AsyncStorage.getItem('client'),
            name: this.state.name,
            city: this.state.city,
            dob: this.state.DOB.substring(0, 10),
            email: this.state.email,
        }).then(res => {
                this.props.refresh();
            }
        )
        ;
    }

    async submitData() {
        let clientData = this.props.clientData;
        if (this.state.DOB == "" || this.state.name == "" || this.state.email == "" || this.state.city == "" || this.state.mobile == "") {
            alert("Все поля должны быть заполнены");
        } else {
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            let Value1 = this.state.mobile.length.toString();
            if (reg.test(this.state.email) === false) {
                alert("Неверный формат почты");
            } else if (Value1 != 11) {
                alert("Номер должен содержать 11 символов")
            } else {
                if (this.state.mobile != clientData[5]) {
                    this.props.sendSMS(this.state.mobile);
                } else {
                    this.props.hideSettings();
                }
                this.acceptChanges();
                Keyboard.dismiss();
            }
        }
    };

    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => {
                        this.props.hideSettings();
                    }}>
                        <Icon ios="ios-arrow-back" android="md-arrow-back" style={{width: 30, height: 30}}/>
                    </TouchableOpacity>

                    <TouchableOpacity style={{position: 'absolute', right: 10}} onPress={() => {
                        this.submitData();
                    }}>
                        <Text style={{fontWeight: 'bold', fontSize: 15}}>Готово</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.content}>

                    <Item style={styles.inputField}>
                        <Input placeholder='Имя'
                               onChangeText={(name) => this.setState({name})} value={this.state.name}/>
                    </Item>

                    <Item style={styles.inputField}>
                        <Input placeholder='Email' keyBoardType="email-address"
                               onChangeText={(email) => this.setState({email})}
                               value={this.state.email}/>
                    </Item>

                    <Item style={styles.inputField}>
                        <Input placeholder='Телефон 8-ХХХ-ХХХ-ХХ-ХХ' keyboardType='phone-pad'
                               onChangeText={(mobile) => this.setState({mobile})}
                               value={this.state.mobile}/>

                    </Item>

                    <View style={styles.dateField}>
                        <DatePicker
                            style={styles.datePicker}
                            date={this.state.DOB}
                            mode="date"
                            placeholder="Дата рождения"
                            androidMode="spinner"
                            format="YYYY-MM-DD"
                            minDate="1950-01-01"
                            maxDate="2019-01-31"
                            confirmBtnText="OK"
                            cancelBtnText="Закрыть"
                            customStyles={{
                                dateInput: {
                                    marginLeft: -DEVICE_WIDTH / 2 - 30,
                                    borderWidth: 0,
                                    borderBottomWidth: 1,
                                },
                                dateText: {
                                    textAlign: 'left',
                                    fontSize: 17,
                                    color: 'black',
                                },
                                placeholderText: {
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: 'grey',
                                }
                            }}
                            onDateChange={(DOB) => {
                                this.setState({DOB: DOB})
                            }}/>
                    </View>

                    <Item style={styles.inputField}>
                        <Input placeholder='Город' onChangeText={(city) => this.setState({city})}
                               value={this.state.city}/>
                    </Item>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    datePicker: {
        width: null,
        marginTop: 15,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        borderColor: 'rgba(255, 255, 255, 0.5)',
        borderRadius: 20,
        overflow: 'hidden'
    },
    inputField: {
        height: 60,
        width: DEVICE_WIDTH,
        marginTop: 30,
        textAlign: 'left',
        marginLeft: 10,
    },
    dateField: {
        height: 60,
        width: DEVICE_WIDTH,
        marginTop: 30,
        textAlign: 'left',
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "flex-start",
        height: 45,
        width: DEVICE_WIDTH,
        paddingLeft: 15,
    },
    content: {
        height: DEVICE_HEIGHT - 80,
    },
});
