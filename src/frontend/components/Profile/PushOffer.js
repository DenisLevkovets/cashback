import React from 'react';
import {StyleSheet, View, TouchableOpacity, Image} from 'react-native';
import {Card, CardItem, Text, Icon, Button} from 'native-base'
import Dimensions from 'Dimensions';
import bgSrc from "../../images/wallpaper.png";

const DEVICE_HEIGHT = Dimensions.get('window').height;


export default class PushOffer extends React.Component {
    constructor(props) {
        super(props);
    }

    //Модель окошка push
    render() {
        var push = this.props.push;
        var call = this.props.hideme;
        var showqr = this.props.showqr;
        return (
            <View style={styles.card}>
                <Card style={styles.modal}>
                    <CardItem header bordered style={styles.header}>
                        <Text>Push</Text>
                        <TouchableOpacity style={{fontSize: 20, position: "absolute", right: 5}} onPress={() => {
                            call();
                            showqr();
                        }}>
                            <Icon ios='ios-qr-scanner' android="md-qr-scanner"/>
                        </TouchableOpacity>
                    </CardItem>

                    <CardItem bordered style={styles.cardbody}>
                        <Text style={{fontWeight: 'bold'}}>{push[0]}  {push[1]}</Text>
                        <Text style={{fontWeight: 'bold',paddingTop: 10}}>Скидка - {push[3]}%</Text>
                        <Text style={{paddingTop: 10}}>Срок действия до - {push[4]}</Text>
                        <Text style={{paddingTop: 10}}>{push[2]}</Text>
                        <Image source={bgSrc} style={{paddingTop:30,marginLeft: 100,width:100,height:70}}/>
                    </CardItem>

                    <CardItem footer bordered style={styles.footer}>
                        <Button style={styles.btnOk}
                                onPress={() => {call()}}>
                            <Text style={styles.text}>Ok</Text>
                        </Button>
                    </CardItem>
                </Card>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

    },
    cardbody: {
         flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        backgroundColor: 'rgba(0,0,0,0)',
        height: 240,
    },
    header: {
        height: 46,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: 'rgba(0,0,0,0.2)',
    },
    footer: {
        height: 45,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        backgroundColor: 'rgba(0,0,0,0)',
    },
    btnOk: {
        height: 46,
        backgroundColor: "rgba(0,0,0,0.2)",
        position: "absolute",
        right: 0,
        left: 0,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        zIndex: 100,
    },
    text: {
        color: "#fff",
        fontSize: 10,
    },
    modal: {
        position: "absolute",
        top: (DEVICE_HEIGHT / 2) - 120,
        width: 320,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
    },



});
