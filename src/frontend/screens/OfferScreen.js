import React from 'react';
import {
    Text,
    ScrollView,
    StyleSheet,
    View,
    SectionList,
    Modal,
    TouchableOpacity,
    AsyncStorage,
    RefreshControl,
} from 'react-native';
import {Item} from 'native-base';
import Dimensions from 'Dimensions';
import Offer from '../components/Profile/Offer';
import QR from '../components/Profile/QR';
import axios from "axios";
import Constants from '../constants/Variables'

const url = Constants.url;

const DEVICE_WIDTH = Dimensions.get('window').width;
const _MS_PER_DAY = 1000 * 60 * 60 * 24;

export default class OfferScreen extends React.Component {
    static navigationOptions = {
        title: 'Предложения',
    };

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            offers: [],
            modalVisible: false,
            qrVisible: false,
            modalItem: [],
            city: "",
            balance: -1,
            tempCity: ""
        };
    }

    //Обновление данных
    _onRefresh = () => {
        this.setState({refreshing: true});
        this.setValues().then(() => {
            this.setState({refreshing: false});
        });
    };

    GetSectionListItem = (item) => {
        this.setState({modalVisible: true, modalItem: item});
    };

    componentDidMount() {
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
            this.setValues();
        });
    }

    async setValues() {
        let city = await AsyncStorage.getItem('city');
        let tempCity = await AsyncStorage.getItem('tempCity');
        this.setState({city: city, balance: await AsyncStorage.getItem('balance'), tempCity: tempCity});
        tempCity.toString().length > 0 ? this.getOffers(tempCity) : this.getOffers(city);
    }


    getOffers(city) {
        axios.post(url + '/offers', {
            city: city,
        })
            .then(res => {
                let offers = [];
                let date = new Date();
                let today = new Date(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
                for (let i = 0; i < res.data.length; i++) {
                    let dbdate = new Date(res.data[i].EndDate.toString().substring(0, 10).replace(/-/g, '/'));
                    let date = new Date(res.data[i].EndDate);
                    let endDate = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
                    let diff = dateDiffInDays(today, dbdate);
                    offers[i] = [res.data[i].Desc, diff, res.data[i].Value, res.data[i].City, res.data[i].ClientValue, endDate,
                        res.data[i].CompanyName, res.data[i].Industry, res.data[i].CompanyValue, res.data[i].SystemValue, res.data[i].Company_idCompany];
                }
                this.setState({offers});
            })
    }

    sortedOffersDate() {
        this.setState({offers: this.state.offers.sort(compareDate)})
    }

    sortedOffersSale() {
        this.setState({offers: this.state.offers.sort(compareSale)})
    }

    hideModal() {
        this.setState({modalVisible: false});
    }

    hideQR() {
        this.setState({qrVisible: false});
    }

    showQR() {
        this.setState({qrVisible: true});
    }

    render() {
        return (
            <ScrollView
                refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh}/>}
                style={{backgroundColor: 'rgba(255,245,70,0.4)'}}>
                <View style={styles.container}>
                    <SectionList
                        sections={[{title: 'Предложения', data: this.state.offers,},]}
                        renderSectionHeader={({section}) =>
                            <Item style={{width: DEVICE_WIDTH, backgroundColor: '#64B5F6'}}>
                                <TouchableOpacity onPress={() => {
                                    this.getOffers(this.state.tempCity.length > 0 ? this.getOffers(this.state.tempCity) : this.getOffers(this.state.city));
                                }}>
                                    <Text style={styles.SectionHeader}> {section.title} </Text>
                                </TouchableOpacity>

                                <Text style={styles.industry}>Отрасль</Text>

                                <TouchableOpacity onPress={() => {
                                    this.sortedOffersDate()
                                }} style={{alignContent: 'center'}}>
                                    <Text style={styles.date}>Дата</Text>
                                </TouchableOpacity>

                                <Text style={styles.rate}>Рейтинг</Text>

                                <TouchableOpacity onPress={() => {
                                    this.sortedOffersSale()
                                }} style={{alignContent: 'center', paddingRight: 10}}>
                                    <Text style={styles.sale}>Скидка</Text>
                                </TouchableOpacity>
                            </Item>
                        }
                        renderItem={({item}) =>
                            <Item style={{width: DEVICE_WIDTH}}>
                                <TouchableOpacity style={{flex: 1, flexDirection: 'row'}}
                                                  onPress={this.GetSectionListItem.bind(this, item)}>
                                    <Text style={styles.SectionListItemS}> {item[6]} </Text>
                                    <Text style={styles.industryValue}> {item[7]} </Text>
                                    <Text style={styles.dateValue}> {item[1]} </Text>
                                    <Text style={styles.rateValue}> 0 </Text>
                                    <Text style={styles.saleValue}> -{item[2]}% </Text>
                                </TouchableOpacity>
                            </Item>
                        }
                        keyExtractor={(item, index) => index}
                    />
                </View>

                <Modal transparent={true}
                       visible={this.state.modalVisible}
                       onRequestClose={() => {
                           this.setState({modalVisible: false});
                           console.log("Modal has been closed.")
                       }}
                       onBackdropPress={() => this.setState({modalVisible: false})}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)',
                    }}>
                        <Offer offer={this.state.modalItem} hideme={this.hideModal.bind(this)}
                               showqr={this.showQR.bind(this)}/>
                    </View>
                </Modal>

                <Modal transparent={true}
                       visible={this.state.qrVisible}
                       onRequestClose={() => {
                           this.setState({qrVisible: false});
                       }}
                       onBackdropPress={() => this.setState({qrVisible: false})}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)',
                    }}>
                        <QR offer={this.state.modalItem} hideqr={this.hideQR.bind(this)}
                            defaultBonus={this.state.balance}/>
                    </View>
                </Modal>
            </ScrollView>
        );
    }
}


function compareDate(offer1, offer2) {
    return offer1[1] - offer2[1];
}

function compareSale(offer1, offer2) {
    return offer2[2] - offer1[2];
}

function dateDiffInDays(a, b) {
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 10,
        justifyContent: "center",
        backgroundColor: "#e5e5e5"
    },
    SectionHeader: {
        backgroundColor: '#64B5F6',
        fontSize: 20,
        padding: 5,
        color: '#fff',
        fontWeight: 'bold',
        alignContent: 'center',
        width: DEVICE_WIDTH - 320,
        height: 40
    },
    SectionListItemS: {
        backgroundColor: '#F5F5F5',
        fontSize: 16,
        padding: 5,
        color: '#000',
        width: DEVICE_WIDTH - 320,
        height: 40
    },
    industryValue: {
        backgroundColor: '#F5F5F5',
        height: 40,
        width: 80,
        alignContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
        paddingRight: 15,
        color: '#000',
    },
    dateValue: {
        backgroundColor: '#F5F5F5',
        height: 40,
        width: 80,
        alignContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
        paddingRight: 15,
        color: '#000',
    },
    rateValue: {
        backgroundColor: '#F5F5F5',
        height: 40,
        width: 80,
        alignContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
        color: '#000',
    },
    saleValue: {
        backgroundColor: '#F5F5F5',
        height: 40,
        width: 80,
        alignContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
        paddingRight: 15,
        color: '#000',
    },
    industry: {
        backgroundColor: '#64B5F6',
        height: 40,
        width: 80,
        alignContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
        color: '#000',
    },
    date: {
        backgroundColor: '#64B5F6',
        height: 40,
        width: 80,
        alignContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
        color: '#000',
    },
    rate: {
        backgroundColor: '#64B5F6',
        height: 40,
        width: 80,
        alignContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
        color: '#c91035',
    },
    sale: {
        backgroundColor: '#64B5F6',
        height: 40,
        width: 80,
        alignContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
        color: '#179a1d',
    },


});
