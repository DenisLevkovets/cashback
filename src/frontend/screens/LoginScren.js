import React from 'react';
import LoginScreen from '../components/Login/LoginScreen'
import Signup from '../components/Signup/Main'
import {AsyncStorage} from 'react-native';
import {Router, Scene, Actions, ActionConst} from 'react-native-router-flux';

export default class LoginScren extends React.Component {
    static navigationOptions = {
        header: null,
    };

    //Проверяем авторизировался ли пользователь ранее
    async componentDidMount() {
        let id=await AsyncStorage.getItem('client');
        let idW=await AsyncStorage.getItem('worker');
        if(id!=null && idW!=null && id>0){
            idW==='-1'?this.props.navigation.navigate('Main'):this.props.navigation.navigate('Worker');
        }
    }

    render() {
        //Навигация между логином и регистрацией
        return (
            <Router>
                <Scene key="root">
                    <Scene key="loginScreen"
                           component={LoginScreen}
                           animation='fade'
                           navigation={this.props.navigation}
                           initial={true}
                    />
                    <Scene key="Signup"
                           component={Signup}
                           navigation={this.props.navigation}
                           animation='fade'
                    />
                </Scene>
            </Router>
        );
    }
}

