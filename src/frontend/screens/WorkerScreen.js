import React from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import ReadQR from "../components/Worker/ReadQR";

export default class WorkerScreen extends React.Component {
    static navigationOptions = {
        title: 'Работник',
    };

    //Считываем qr код
    render() {
        return (
            <ScrollView style={styles.container}>
                <ReadQR/>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: 'rgba(255,240,70,0.4)',
    },
});
