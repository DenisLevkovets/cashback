import React from 'react';
import {
    AsyncStorage,
    Image, Modal,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,

    FlatList, RefreshControl,
} from 'react-native';
import logoImg from '../images/cashback.png';
import Header from '../components/Profile/Header';
import Deal from "../components/Deal/Deal";
import Constants from '../constants/Variables'
import axios from "axios";
import QR from "../components/Profile/QR";
import ProfileSettings from "../components/Profile/ProfileSettings";

const url = Constants.url;

export default class HistoryScreen extends React.Component {
    static navigationOptions = {
        title: "История покупок",
    };

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            deals: [],
            modalVisible: false,
            qrVisible: false,
            modalItem: [],
            clientData:[],
            balance: -1,
            rates:[],
            tempCity:"",
            settingsVisible:false
        };
    }

    //Обновление страницы
    _onRefresh = () => {
        this.setState({refreshing: true});
        this.getBalance().then(() => {
            this.getRate();
            this.getHistory(this.state.clientData[1]);
            this.setState({refreshing: false});
        });
    };

    hideModal() {
        this.setState({modalVisible: false});
    }

    showModal(item) {
        this.setState({modalVisible: true, modalItem: item});
    }

    convertMonth(month) {
        switch (month) {
            case '01':
                return "января";
            case '02':
                return "февраля";
            case '03':
                return "марта";
            case '04':
                return "апреля";
            case '05':
                return "мая";
            case '06':
                return "июня";
            case '07':
                return "июля";
            case '08':
                return "августа";
            case '09':
                return "сентября";
            case '10':
                return "октября";
            case '11':
                return "ноября";
            case '12':
                return "декабря";
        }
    }

    //Получаем историю
    async getHistory(city) {
        let idClient=await AsyncStorage.getItem('client');
        axios.post(url + '/history', {
            city: city,
            idClient: idClient,
        })
            .then(res => {
                //Собираем нужные нам данные в двумерный массив покупок и их содержимого
                let deals = [];
                for (let i = 0; i < res.data.length; i++) {
                    let date = res.data[i].Date.toString().substring(8, 10) + " " + this.convertMonth(res.data[i].Date.toString().substring(5, 7)) + " " + res.data[i].Date.toString().substring(0, 4) + " " + (parseInt(res.data[i].Date.toString().substring(11, 13)) + 3).toString() + ":" + res.data[i].Date.toString().substring(14, 16);
                    let receivedbonus = Math.round((res.data[i].Value / 100) * res.data[i].Bonus);
                    deals[i] = [date, res.data[i].Company, res.data[i].Industry, res.data[i].Value, receivedbonus, res.data[i].UsedBonus,
                        res.data[i].Sale, 0, 0, res.data[i].City, res.data[i].idDeal, res.data[i].Company_idCompany, res.data[i].Worker_idWorker, idClient]
                }
                //Сортируем список по дате
                this.setState({deals: deals.sort(sortDate)});
            })
    }

    //Получаем список оценок
    getRate() {
        axios.post(url + '/returnRates')
            .then(res => {
                //Аналогично собираем в двумерный массив
                let rates = [];
                for (let i = 0; i < res.data.length; i++) {
                    rates[i] = [res.data[i].Rate,res.data[i].Deal_idDeal,res.data[i].Client_idClient]
                }
                this.setState({rates});
            }
        )
    }

    //Получаем имя и город и сразу вызываем получение истории по этому городу
    async getName() {
        axios.post(url + '/name', {
            id: await AsyncStorage.getItem('client'),
        }).then(res => {
            let clientData = [res.data[0].FullName, res.data[0].City, res.data[0].TempCity, res.data[0].DOB,res.data[0].Email,res.data[0].Phone];
            this.setState({clientData});
            this.getHistory(clientData[1]);
        });
    }

    //Получаем баланс
    async getBalance() {
        axios.post(url + '/balance', {
            idCard: await AsyncStorage.getItem('card'),
        }).then(res => {
            const balance = res.data[0].Balance;
            this.setState({balance});
        });
    }

    //Вызываем сразу
    componentDidMount() {
        this.getName();
        this.getBalance();
        this.getRate();
    }

    showSimpleQR() {
        this.setState({qrVisible: true, modalItem: []});
    }

    hideSettings(){
        this.setState({settingsVisible: false})
    }

    showSettings() {
        this.setState({settingsVisible: true});
    }

    hideQR() {
        this.setState({qrVisible: false});
    }

    //Выходим и очищаем хранилище
    logout() {
        AsyncStorage.clear();
        this.props.navigation.navigate('Login');
    }

    setTempCity(city) {
        this.setState({tempCity: city});
    }

    //Ищем оценку в этом предложении
    returnRate(idDeal,idClient){
        let rates=this.state.rates;
        for (let i = 0; i < rates.length; i++) {
            if (rates[i][1]==idDeal && rates[i][2]==parseInt(idClient)) {
                return rates[i][0];
            }
        }
        return 0;
    };

    render() {
        return (
            <ScrollView refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh}/>} style={styles.container}>
                <Header clientData={this.state.clientData} balance={this.state.balance}
                        showqr={this.showSimpleQR.bind(this)} logout={this.logout.bind(this)}
                        setTempCity={this.setTempCity.bind(this)} refresh={this.getHistory.bind(this)}
                        showSettings={this.showSettings.bind(this)}/>
                <FlatList
                    style={{marginLeft: 10, marginTop: 20}}
                    data={this.state.deals}
                    renderItem={({item}) =>
                        <TouchableOpacity onPress={() => {
                            this.showModal(item)
                        }}>
                            <View>
                                <Text style={{fontWeight: 'bold', fontSize: 17}}>{item[0]}</Text>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <Image source={logoImg} style={styles.image}/>
                                    <View style={{paddingLeft: 50}}>
                                        <View style={styles.section}>
                                            <Text style={styles.h1}>{item[2]} {item[1]}</Text>
                                            <Text style={styles.h2}>{item[9]}</Text>
                                        </View>
                                        <View style={styles.section}>
                                            <Text style={styles.h1}>0</Text>
                                            <Text style={styles.h2}>Рейтинг</Text>
                                        </View>
                                        <View style={styles.section}>
                                            <Text style={styles.h1}>{item[3]} руб</Text>
                                            <Text style={styles.h2}>Сумма покупки</Text>
                                        </View>
                                        <View style={styles.section}>
                                            <Text style={styles.h1}>{item[4]}/{item[5]}</Text>
                                            <Text style={styles.h2}>Начислено/Потрачено</Text>
                                        </View>
                                        <View style={styles.section}>
                                            <Text style={styles.h1}>{item[6]}%</Text>
                                            <Text style={styles.h2}>Скидка</Text>
                                        </View>
                                        <View style={styles.section}>
                                            <Text style={styles.h1}>{this.returnRate(item[10],item[13])}/5</Text>
                                            <Text style={styles.h2}>Оценка</Text>
                                        </View>
                                        <View style={styles.section}>
                                            <Text style={styles.h1}>{item[8]}</Text>
                                            <Text style={styles.h2}>Чаевые</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    }
                    keyExtractor={item => item[10].toString()}
                />

                <Modal transparent={true}
                       visible={this.state.modalVisible}
                       onRequestClose={() => {
                           this.setState({modalVisible: false});
                           console.log("Modal has been closed.")
                       }}
                       onBackdropPress={() => this.setState({modalVisible: false})}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)',
                    }}>
                        <Deal hideme={this.hideModal.bind(this)} deal={this.state.modalItem}
                              balance={this.state.balance}/>
                    </View>
                </Modal>

                <Modal transparent={true}
                       visible={this.state.qrVisible}
                       onRequestClose={() => {
                           this.setState({qrVisible: false});
                       }}
                       onBackdropPress={() => this.setState({qrVisible: false})}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)',
                    }}>
                        <QR offer={[]} hideqr={this.hideQR.bind(this)} defaultBonus={this.state.balance} city={this.state.city}/>
                    </View>
                </Modal>

                <Modal transparent={true}
                           visible={this.state.settingsVisible}
                           onRequestClose={() => {
                               this.setState({settingsVisible: false});
                           }}
                           onBackdropPress={() => this.setState({settingsVisible: false})}
                    >
                        <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#fff',
                    }}>
                            <ProfileSettings clientData={this.state.clientData} hideSettings={this.hideSettings.bind(this)}/>
                        </View>
                    </Modal>
            </ScrollView>


        );
    }
}

function sortDate(deal1, deal2) {
    if (deal2[0] > deal1[0]) return 1;
    if (deal2[0] < deal1[0]) return -1;
    return 0;
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(255,240,70,0.4)',
    },
    h1: {
        fontSize: 12,
    },
    h2: {
        fontSize: 10,
        color: "#8d8d8d",
    },
    section: {
        paddingTop: 10,
    },
    image: {
        width: 120,
        height: 120,
    },

});
