import React from 'react';
import {
    Text,
    ScrollView,
    StyleSheet,
    View,
    SectionList,
    Modal,
    AsyncStorage,
    TouchableOpacity,
    RefreshControl
} from 'react-native';
import {Notifications,} from 'expo';
import {Item, Icon} from 'native-base';
import Dimensions from 'Dimensions';
import Header from '../components/Profile/Header';
import Offer from '../components/Profile/Offer';
import QR from '../components/Profile/QR';
import ProfileSettings from "../components/Profile/ProfileSettings";
import SMS from '../components/Signup/SMS'
import axios from "axios";
import Constants from '../constants/Variables'
import PushOffer from "../components/Profile/PushOffer";

const url = Constants.url;

const DEVICE_WIDTH = Dimensions.get('window').width;

export default class ProfileScreen extends React.Component {
    static navigationOptions = {
        title: 'Профиль',
    };

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            offers: [],
            messages: [],
            balance: -1,
            modalVisible: false,
            qrVisible: false,
            pushVisible: false,
            modalItem: [],
            pushItem: [],
            clientData: [],
            tempCity: "",
            settingsVisible: false,
            smsVisible: false,
            SMSText: "",
            number: "",
        };
    }

    //Обновление данных
    _onRefresh = () => {
        this.setState({refreshing: true});
        this.getName();
        this.getMessages();
        this.getBalance().then(() => {
            this.state.tempCity === "" ? this.getOffers(this.state.clientData[1]) : this.getOffers(this.state.tempCity);
            this.setState({refreshing: false});
        });
    };

    //Показ модели предложение и передача данных в modalItem
    GetSectionListItem = (item) => {
        this.setState({modalVisible: true, modalItem: item});
    };

    GetSectionListPushItem = (item) => {
        this.setState({pushVisible: true, pushItem: item});
    };

    //Запрос получения данных из таблицы Client для редактирования профиля
    async getName() {
        axios.post(url + '/name', {
            id: await AsyncStorage.getItem('client'),
        }).then(res => {
            let clientData = [res.data[0].FullName, res.data[0].City, res.data[0].TempCity, res.data[0].DOB, res.data[0].Email, res.data[0].Phone];
            this.setState({clientData: clientData, tempCity: clientData[2]});
            this.getOffers(clientData[2] == null ? clientData[1] : clientData[2]);
            AsyncStorage.setItem("city", clientData[1].toString())
        });
    }

    //Запрос получения списка предложений из таблицы Offer
    getOffers(curCity) {
        axios.post(url + '/offers', {
            city: curCity
        })
            .then(res => {
                //Фильтруем ответ с бэкэнда и помещаем всё в список предложений
                let offers = [];
                let length = res.data.length > 10 ? 10 : res.data.length;
                for (let i = 0; i < length; i++) {
                    let date = new Date(res.data[i].EndDate);
                    let endDate = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
                    offers[i] = [res.data[i].Desc, "", res.data[i].Value, res.data[i].City, res.data[i].ClientValue, endDate,
                        res.data[i].CompanyName, res.data[i].Industry, res.data[i].CompanyValue, res.data[i].SystemValue, res.data[i].Company_idCompany];
                }
                this.setState({offers});

            })
    }

    async getMessages() {
        axios.post(url + '/messages', {
            idClient: await AsyncStorage.getItem('client')
        }).then(res => {
            let messages = [];
            for (let i = 0; i < res.data.length; i++) {
                let date = new Date(res.data[i].EndDate);
                let endDate = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
                messages[i] = [res.data[i].Industry, res.data[i].CompanyName, res.data[i].Text, res.data[i].Price, endDate, res.data[i].From, res.data[i].idMessage];
            }
            this.setState({messages});
            console.log(messages)
        })
    }

    //Запрос получения баланса из таблицы Card
    async getBalance() {
        axios.post(url + '/balance', {
            idCard: await AsyncStorage.getItem('card'),
        }).then(res => {
            const balance = res.data[0].Balance;
            this.setState({balance});
            AsyncStorage.setItem("balance", balance.toString())
        });
    }

    //Метод,который выполняется автоматически при первоначальном вызове класса
    componentDidMount() {
        this.getName();
        this.getBalance();
        this.getMessages();
        this._notificationSubscription = Notifications.addListener(this._handleNotification);
    }

    _handleNotification = (notification) => {
        this.getMessages();
    };

    //Показ/спрятывание моделей предложения и qr кода
    hideModal() {
        this.setState({modalVisible: false});
    }

    hidePush() {
        this.setState({pushVisible: false});
    }

    async setTempCity(city) {
        axios.post(url + '/setTempCity', {id: await AsyncStorage.getItem('client'), city: city});
        this.setState({tempCity: city});
        AsyncStorage.setItem("tempCity", city)
    }

    hideQR() {
        this.setState({qrVisible: false});
    }

    showQR() {
        this.setState({qrVisible: true});
    }

    hideSMS() {
        this.setState({smsVisible: false});
    }


    setSMSText(value) {
        this.setState({inputValue: value})
    }

    //Отправка смс
    sendSMS(number) {
        this.setState({number: number});
        axios.post(url + '/sms', {
            number: number
        }).then(res => {
            if (res.status === 200) {
                if (res.data === "Можно отправлять только одно сообщение в день") {
                    alert(res.data)
                }
                else {
                    this.setState({smsVisible: true, SMSText: res.data})
                }
            }
        })
    }

    //Проверяем кол из смс
    async checkSMS() {
        if (this.state.SMSText === this.state.inputValue) {
            axios.post(url + '/updateClientPhone', {
                id: await AsyncStorage.getItem('client'),
                phone: this.state.number
            });
            AsyncStorage.setItem('phone', this.state.number);
            this.setState({smsVisible: false, settingsVisible: false});
        } else {
            alert("Неправильный код");
        }
    }

    hideSettings() {
        this.setState({settingsVisible: false})
    }

    showSettings() {
        this.setState({settingsVisible: true});
    }

    showSimpleQR() {
        this.setState({qrVisible: true, modalItem: []})
    }

    // Очищаем все данные локального хранилища и выходим
    async logout() {
        AsyncStorage.removeItem('card');
        AsyncStorage.removeItem('client');
        AsyncStorage.removeItem('worker');
        AsyncStorage.removeItem('city');
        AsyncStorage.removeItem('tempCity');
        AsyncStorage.removeItem('balance');

        this.props.navigation.navigate('Login');
    }

    render() {
        return (
            <ScrollView
                refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh}/>}
                style={{backgroundColor: 'rgba(255,245,70,0.4)'}}>
                <Header clientData={this.state.clientData} balance={this.state.balance}
                        showqr={this.showSimpleQR.bind(this)} logout={this.logout.bind(this)}
                        setTempCity={this.setTempCity.bind(this)} refresh={this.getOffers.bind(this)}
                        showSettings={this.showSettings.bind(this)}
                />
                <View style={styles.container}>
                    <SectionList
                        sections={[
                            {
                                title: 'Push уведомления',
                                data: this.state.messages,
                            },
                        ]}
                        renderSectionHeader={({section}) =>
                            <Item style={{width: DEVICE_WIDTH, backgroundColor: '#64B5F6'}}>
                                <Text style={styles.SectionHeaderPush}> {section.title} </Text>
                                <Text style={styles.sale}>Скидка</Text>
                            </Item>
                        }
                        renderItem={({item}) =>
                            <Item style={{width: DEVICE_WIDTH}}>
                                <TouchableOpacity style={{flex: 1, flexDirection: 'row'}}
                                                  onPress={this.GetSectionListPushItem.bind(this, item)}>
                                    <Text style={styles.SectionListItemPush}> {item[1]} </Text>
                                    <Text style={styles.industryPush}> {item[0]} </Text>
                                    <Text style={styles.saleValue}> {item[3]}% </Text>
                                </TouchableOpacity>
                            </Item>
                        }
                        keyExtractor={(item, index) => index}
                    />
                    <SectionList
                        sections={[
                            {
                                title: 'TOP-10',
                                data: this.state.offers,
                            },

                        ]}
                        renderSectionHeader={({section}) =>
                            <Item style={{width: DEVICE_WIDTH, backgroundColor: '#64B5F6'}}>
                                <Text style={styles.SectionHeader}> {section.title} </Text>
                                <Text style={styles.rate}>Рейтинг</Text>
                                <Text style={styles.sale}>Скидка</Text>
                            </Item>
                        }
                        renderItem={({item}) =>
                            <Item style={{width: DEVICE_WIDTH}}>
                                <TouchableOpacity style={{flex: 1, flexDirection: 'row'}}
                                                  onPress={this.GetSectionListItem.bind(this, item)}>
                                    <Text style={styles.SectionListItemS}> {item[6]} </Text>
                                    <Text style={styles.industry}> {item[7]} </Text>
                                    <Text style={styles.rateValue}> 0 </Text>
                                    <Text style={styles.saleValue}> -{item[2]}% </Text>
                                </TouchableOpacity>
                            </Item>
                        }
                        keyExtractor={(item, index) => index}
                    />
                    <Item style={{
                        flex: 1,
                        flexDirection: 'row',
                        alignItems: "flex-start",
                        height: 35,
                        width: DEVICE_WIDTH,
                    }}>
                        <Text style={{
                            fontWeight: 'bold',
                            fontSize: 17,
                            width: DEVICE_WIDTH - 45,
                            height: 35,
                            paddingTop: 5
                        }}>Выход</Text>
                        <TouchableOpacity onPress={() => {
                            this.logout();
                        }}>
                            <Icon ios='ios-log-out' android='md-log-out' style={{fontSize: 32}}/>
                        </TouchableOpacity>
                    </Item>
                </View>

                <Modal transparent={true}
                       visible={this.state.modalVisible}
                       onRequestClose={() => {
                           this.setState({modalVisible: false});
                       }}
                       onBackdropPress={() => this.setState({modalVisible: false})}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)',
                    }}>
                        <Offer offer={this.state.modalItem} hideme={this.hideModal.bind(this)}
                               showqr={this.showQR.bind(this)}/>
                    </View>
                </Modal>

                <Modal transparent={true}
                       visible={this.state.qrVisible}
                       onRequestClose={() => {
                           this.setState({qrVisible: false});
                       }}
                       onBackdropPress={() => this.setState({qrVisible: false})}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0)',
                    }}>
                        <QR offer={this.state.modalItem} push={this.state.pushItem} hideqr={this.hideQR.bind(this)}
                            defaultBonus={this.state.balance} city={this.state.clientData[1]}/>
                    </View>
                </Modal>

                <Modal transparent={true}
                       visible={this.state.pushVisible}
                       onRequestClose={() => {
                           this.setState({pushVisible: false});
                       }}
                       onBackdropPress={() => this.setState({pushVisible: false})}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0)',
                    }}>
                        <PushOffer push={this.state.pushItem} hideme={this.hidePush.bind(this)}
                                   showqr={this.showQR.bind(this)}/>
                    </View>
                </Modal>

                <Modal transparent={true}
                       visible={this.state.settingsVisible}
                       onRequestClose={() => {
                           this.setState({settingsVisible: false});
                       }}
                       onBackdropPress={() => this.setState({settingsVisible: false})}
                >
                    <View style={{
                        flex: 1,
                        backgroundColor: '#fff',
                    }}>
                        <ProfileSettings clientData={this.state.clientData} refresh={this._onRefresh.bind(this)}
                                         hideSettings={this.hideSettings.bind(this)} sendSMS={this.sendSMS.bind(this)}/>
                    </View>
                </Modal>

                <Modal transparent={true}
                       visible={this.state.smsVisible}
                       onRequestClose={() => {
                       }}
                >
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)',
                    }}>
                        <SMS checkSMS={this.checkSMS.bind(this)} hideSMS={this.hideSMS.bind(this)}
                             setSMSText={this.setSMSText.bind(this)}/>
                    </View>
                </Modal>

            </ScrollView>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 10,
        justifyContent: "center",
        backgroundColor: "#e5e5e5"
    },
    SectionHeader: {
        backgroundColor: '#64B5F6',
        fontSize: 20,
        padding: 5,
        color: '#fff',
        fontWeight: 'bold',
        alignContent: 'center',
        width: DEVICE_WIDTH - 160,
        height: 40
    },
    SectionHeaderPush: {
        backgroundColor: '#64B5F6',
        fontSize: 20,
        padding: 5,
        color: '#fff',
        fontWeight: 'bold',
        alignContent: 'center',
        width: DEVICE_WIDTH - 80,
        height: 40
    },
    SectionListItemS: {
        backgroundColor: '#F5F5F5',
        fontSize: 16,
        padding: 5,
        color: '#000',
        width: DEVICE_WIDTH - 240,
        height: 40
    },
    SectionListItemPush: {
        backgroundColor: '#F5F5F5',
        fontSize: 16,
        padding: 5,
        color: '#000',
        width: DEVICE_WIDTH - 240,
        height: 40
    },
    rateValue: {
        backgroundColor: '#F5F5F5',
        height: 40,
        width: 80,
        alignContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
        color: '#000',
    },
    saleValue: {
        backgroundColor: '#F5F5F5',
        height: 40,
        width: 80,
        alignContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
        paddingRight: 15,
        color: '#000',
    },
    industry: {
        backgroundColor: '#F5F5F5',
        height: 40,
        width: 80,
        paddingTop: 10,
        color: '#000',
    },
    industryPush: {
        backgroundColor: '#F5F5F5',
        height: 40,
        width: 160,
        paddingTop: 10,
        color: '#000',
    },
    rate: {
        backgroundColor: '#64B5F6',
        height: 40,
        width: 80,
        alignContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
        color: '#c91035',
    },
    sale: {
        backgroundColor: '#64B5F6',
        height: 40,
        width: 80,
        alignContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
        color: '#179a1d',
    },


});
