const express = require('express');
const app = express();
var SMSru = require('sms_ru');
const mysql = require('mysql');
const bodyParser = require('body-parser');
const cors = require('cors');
const {Expo} = require('expo-server-sdk');

const port = 3210;

app.use(bodyParser.json());
app.use(cors());

const db = mysql.createConnection({
    host: 'localhost',
    user: 'denis',
    password: 'denis',
    database: 'mydb'
});

db.connect();


//Создание пользователя
app.post('/create', function (req, res) {

    let dataCard = [
        0,
        new Date(),
        req.body.city
    ];

    let sql = "INSERT INTO Card(Balance,RegDate,City) VALUES ('?',?,?)";
    //Создаём карту
    db.query(sql, dataCard, (err, result) => {
        if (err) throw err;

        let dataClient = [
            req.body.name + ' ' + req.body.surname,
            new Date(),
            req.body.city,
            req.body.dob,
            req.body.email,
            req.body.phone,
            result.insertId
        ];

        //Создаём клеинта
        sql = "INSERT INTO Client(FullName,RegDate,City,DOB,Email,Phone,Card_idCard) VALUES (?,?,?,?,?,?,'?')";
        db.query(sql, dataClient, (err, resul) => {
            if (err) throw err;
            let md5 = require('md5');
            //Создаём юзера
            db.query("INSERT INTO User(phone,psw,Ref,Client_idClient) VALUES('" + req.body.phone + "' , '" + md5(req.body.pass) + "','10','" + resul.insertId + "')");
        });
        res.send({
            status: 'Success',
            idCard: result.insertId
        });
    });
});

//Вход
app.post('/login', function (req, res) {

    let md5 = require('md5');
    let sqlUser = "SELECT idUser, Client_idClient FROM User WHERE phone= '" + req.body.phone + "' AND psw='" + md5(req.body.pass) + "' ";
    db.query(sqlUser, (err, result) => {
        if (err) throw err;
        if (result.length === 0) res.send({status: "not ok"});
        else {
            let sqlWorker = "SELECT idWorker FROM Worker WHERE Client_idClient = '" + result[0].Client_idClient + "' ";
            db.query(sqlWorker, (err, resul) => {
                if (err) throw err;
                let sqlClient = "SELECT Card_idCard FROM Client WHERE idClient ='" + result[0].Client_idClient + "' ";
                db.query(sqlClient, (err, resu) => {
                    //Если это работник
                    if (resul.length !== 0) res.send({
                        status: "worker",
                        id: result[0].Client_idClient,
                        idW: resul[0].idWorker,
                        idC: resu[0].Card_idCard
                    });
                    //Если это клиент
                    else if (result.length !== 0) res.send({
                        status: "client",
                        id: result[0].Client_idClient,
                        idC: resu[0].Card_idCard,
                        idW: -1
                    });
                    else res.send({status: "not ok"});
                })
            });
        }
    });
});

//Обновить данные клиента
app.post('/updateClient', function (req, res) {
    let sql = "UPDATE Client SET FullName='" + req.body.name + "', City='" + req.body.city + "', DOB='" + req.body.dob + "', Email='" + req.body.email + "' WHERE idClient='" + req.body.id + "'";
    db.query(sql, (err, result) => {
        if (err) throw err;
        res.send(result);
    })
});

//Обновить данные клиента
app.post('/updateClientPhone', function (req, res) {
    let sql = "UPDATE Client SET Phone='" + req.body.phone + "' WHERE idClient='" + req.body.id + "'";
    db.query(sql, (err) => {
        if (err) throw err;
    });

    let sqlUser = "UPDATE User SET Phone='" + req.body.phone + "' WHERE Client_idClient='" + req.body.id + "'";
    db.query(sqlUser, (err, result) => {
        if (err) throw err;
        res.send(result);
    });
});

//Возвращаем баланс клиента по карте
app.post('/balance', function (req, res) {
    let sql = "SELECT * FROM Card WHERE idCard='" + req.body.idCard + "'";
    db.query(sql, (err, result) => {
        if (err) throw err;
        if (result.length !== 0) res.send(result);
        else res.send({status: "not ok"})
    });
});

//Возвращаем имя и город
app.post('/name', function (req, res) {
    let sql = "SELECT FullName,City,TempCity,DOB,Email,Phone FROM Client WHERE idClient='" + req.body.id + "'";
    db.query(sql, (err, result) => {
        if (err) throw err;
        if (result.length !== 0) res.send(result);
        else res.send({status: "not ok"})
    });
});

//Список предложений
app.post('/offers', function (req, res) {
    let date = new Date();
    let today = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    let sql = "SELECT * FROM Offer JOIN BonusHandbook ON Offer.BonusHandbook_idBonus=BonusHandbook.idBonus JOIN Company ON Company.idCompany=Offer.Company_idCompany WHERE City='" + req.body.city + "' AND Offer.EndDate>'" + today + "'";
    db.query(sql, (err, result) => {
        if (err) throw err;
        res.send(result);

    });
});

//Список push уведомлений
app.post('/messages', function (req, res) {
    let date = new Date();
    let today = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    let sql = "SELECT * FROM Message JOIN Company ON Message.From=Company.idCompany WHERE Message.To='" + req.body.idClient + "' AND Message.EndDate>'" + today + "' AND Message.isCanceled=0";
    db.query(sql, (err, result) => {
        if (err) throw err;
        res.send(result);

    });
});

app.post('/checkPush', function (req, res) {
    let check = "SELECT isCanceled FROM Message WHERE idMessage='" + req.body.idmessage + "'";
    db.query(check, (err, result) => {
        if (err) throw err;
        res.send(result);
    });
});

//Сделка
app.post('/deal', function (req, res) {
    console.log('wwwww');
    if (req.body.status === 'Предложение') {
        //Бонусы клиенту
        let sum = Math.round((req.body.sum / 100) * req.body.clientbonus) - req.body.usedbonus;
        let sqlClient = "UPDATE Card SET Balance=Balance+'" + sum + "' WHERE idCard='" + req.body.idCard + "' ";
        db.query(sqlClient);

        //Бонусы кассиру
        let sqlWorker = "UPDATE Card SET Balance=Balance+'" + Math.round((req.body.sum / 100) * req.body.workerbonus) + "' WHERE idCard='" + req.body.idWorkerCard + "' ";
        db.query(sqlWorker);

        //Снимаем бонус с компании
        let companysum = Math.round((req.body.sum / 100) * req.body.clientbonus) + Math.round((req.body.sum / 100) * req.body.workerbonus) + Math.round((req.body.sum / 100) * req.body.systembonus);
        let sqlCompany = "UPDATE Company SET Balance=Balance-'" + companysum + "' WHERE idCompany='" + req.body.idCompany + "' ";
        db.query(sqlCompany);
    }

    if (req.body.status === 'Пуш') {
        let cancel = "UPDATE Message SET isCanceled=1 WHERE idMessage='" + req.body.idmessage + "'";
        db.query(cancel);
    }

    let usedBonus = "UPDATE Card SET Balance=Balance-'" + req.body.usedbonus + "' WHERE idCard='" + req.body.idCard + "'";
    db.query(usedBonus);
    let date = new Date();
    let dataDeal = [
        date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + " " + date.getHours() + ":" + date.getMinutes(),
        req.body.sum,
        req.body.clientbonus,
        req.body.usedbonus,
        req.body.company,
        req.body.industry,
        req.body.city,
        req.body.idClient,
        req.body.idWorker,
        req.body.value,
        req.body.idCompany
    ];
    let sql = "INSERT INTO Deal(Date,Value,Bonus,UsedBonus,Company,Industry,City,Client_idClient,Worker_idWorker,Sale,Company_idCompany) VALUES (?,?,?,?,?,?,?,?,?,?,?) ";
    //Создаем запись сделки
    db.query(sql, dataDeal, (err, result) => {
        if (err) throw err;
        //Бонус системе
        db.query("INSERT INTO System(Bonus,Deal_idDeal) VALUES (?,?)", [Math.round((req.body.sum / 100) * req.body.systembonus), result.insertId]);
        res.send(result);
    });
});

//Возвращаем историю покупок клиента по городу
app.post('/history', function (req, res) {
    let sql = "SELECT * FROM Deal WHERE Client_idClient='" + req.body.idClient + "' AND City='" + req.body.city + "'";
    db.query(sql, (err, result) => {
        if (err) throw err;
        res.send(result);
    });
});

//Создаём запись оценки
app.post('/rate', function (req, res) {
    db.query("INSERT INTO Rate(Rate,Client_idClient,Client_Card_idCard,Company_idCompany,Deal_idDeal) VALUES ('?',?,?,?,?)",
        [req.body.Rate, req.body.idClient, req.body.idCard, req.body.idCompany.toString(), req.body.idDeal.toString()], (err, result) => {
            if (err) throw err;
            res.send(result);
        });
});

//Проверем,поставлена ли уже оценка
app.post('/checkRate', function (req, res) {
    db.query("SELECT Rate FROM Rate WHERE Client_idClient='" + req.body.idClient + "' AND Deal_idDeal='" + req.body.idDeal + "'", (err, result) => {
        if (err) throw err;
        res.send(result);
    });
});

//Возвращаем все оценки,чтобы вывести их на экране истории покупок
app.post('/returnRates', function (req, res) {
    db.query("SELECT Rate,Deal_idDeal,Client_idClient FROM Rate ", (err, result) => {
        if (err) throw err;
        res.send(result);
    });
});

//Списываем склиента чаевые и отправляем их работнику
app.post('/tip', function (req, res) {
    //Ищем карту работника
    db.query("SELECT Client_Card_idCard FROM Worker WHERE idWorker='" + req.body.idWorker + "'", (err, result) => {
        if (err) throw err;
        let sqlClient = "UPDATE Card SET Balance=Balance-'" + req.body.Tip + "' WHERE idCard='" + req.body.idCard + "' ";
        db.query(sqlClient, (err, result) => {
            if (err) throw err;
        });

        let sqlWorker = "UPDATE Card SET Balance=Balance+'" + req.body.Tip + "' WHERE idCard='" + result[0].Client_Card_idCard + "' ";
        db.query(sqlWorker, (err, result) => {
            if (err) throw err;
        });
        res.send(result)
    });
});

app.post('/refBonus', function (req, res) {
    let sqlClientCard = "SELECT Card_idCard FROM Client WHERE Phone='" + req.body.phone + "' ";
    db.query(sqlClientCard, (err, result) => {
        let sqlClientBonus = "UPDATE Card SET Balance=Balance+'100' WHERE idCard='" + result[0].Card_idCard + "'";
        db.query(sqlClientBonus);
        let sqlWorker = "SELECT Company_idCompany FROM Worker WHERE Client_Card_idCard='" + result[0].Card_idCard + "'";
        db.query(sqlWorker, (er, resul) => {
            if (resul.length !== 0) {
                let sqlCompanyBonus = "UPDATE Company SET Balance=Balance+'100' WHERE idCompany='" + resul[0].Company_idCompany + "'";
                db.query(sqlCompanyBonus);
            }
        });
    });
    let sqlYourBonus = "UPDATE Card SET Balance=Balance+'100' WHERE idCard='" + req.body.idCardOwn + "'";
    db.query(sqlYourBonus);
});


app.post('/companyInfo', function (req, res) {
    let sqlWorker = "SELECT Company_idCompany FROM Worker WHERE idWorker='" + req.body.idW + "'";
    db.query(sqlWorker, (err, result) => {
        if (err) throw err;
        let sqlCompany = "SELECT idCompany,CompanyName,Industry FROM Company WHERE idCompany='" + result[0].Company_idCompany + "'";
        db.query(sqlCompany, (err, result) => {
            if (err) throw err;
            res.send(result);
        });
    });

});

app.post('/sms', function (req, res) {
    let text = "";
    let possible = "abcdefghijklmno0pqrs1tuvwx2yzA3BCD4EF5GH6IG7KLM8NOP9QRSTUVWXYZ";

    for (var i = 0; i < 7; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    //api_id
    let sms = new SMSru('FE9E09F2-5588-C24D-06DC-A0C95986BAD2');


    let send = false;
    let checksms = "SELECT * FROM SMS WHERE Phone='" + req.body.number + "' ORDER BY Date DESC";
    db.query(checksms, (err, result) => {
        if (err) throw err;
        if (result.length === 0) {
            send = true;
        }
        else {
            if (dateDiffInDays(result[0].Date, new Date()) > 0) {
                send = true;
            }
        }
        console.log(text);

        if (send === true) {
            sms.sms_send({
                to: req.body.number,
                text: text
            }, function (e) {
                console.log(e.description)
            });
            let sql = "INSERT INTO SMS(Phone,Date) VALUES (?,?)";
            db.query(sql, [req.body.number, new Date()]);
        }
        else {
            text = "Можно отправлять только одно сообщение в день"
        }
        res.send(text)

    });

});

app.post('/setTempCity', function (req, res) {
    let sql = "UPDATE Client SET TempCity='" + req.body.city + "' WHERE idClient='" + req.body.id + "'";
    db.query(sql, (err, result) => {
        if (err) throw err;
        res.send(result)
    });
});

app.post('/registerToken', function (req) {
    let check = "SELECT * FROM Push WHERE Client_idClient='" + req.body.idClient + "' ";
    db.query(check, (err, result) => {
        if (err) throw err;
        if (result.length === 0) {
            let sql = "INSERT INTO Push(Token,Client_idClient) VALUES (?, ?)";
            db.query(sql, [req.body.token, req.body.idClient]);
        }
    });

});


function dateDiffInDays(a, b) {
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

app.listen(port, () => {
    console.log('Server works on port 3210')
});


